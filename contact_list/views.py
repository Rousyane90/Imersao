from django.shortcuts import render, redirect
from .models import Contact
from .forms import ContactForm
from django.http import HttpResponseRedirect

def register(request):
        form = ContactForm(request.POST)
        if request.method == "POST":
            if form.is_valid():
                form.save()
                return redirect('index')
        return render(request, 'edit_create.html', {'form':form})

def index(request):
    contato = Contact.objects.all()
    context = {'contatos':contato}
    return render(request, 'index.html', context)