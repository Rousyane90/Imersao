from django.urls import path
from .views import register,index

urlpatterns = [
    path(r'register', register, name="register"),
    path(r'index', index, name="index"),
]
